# Installation de Git et configuration de GitLab

## Git

<img align="right" height="100" src="figs/logos/logo-git.png" />

Git est un logiciel de gestion de versions décentralisé.

Comme l'affirme [Wikipédia](https://fr.wikipedia.org/wiki/Git) (Belle référence ! :sweat_smile:) :

> C'est un logiciel libre créé par Linus Torvalds, auteur du noyau Linux. Depuis 2016, il s’agit du logiciel de gestion de versions le plus populaire qui est utilisé par plus de douze millions de personnes.

La principale composante de Git sur Windows est Git Bash, qui est une implémentation de console digne d'une console Linux. Mais surtout, l'utilisation première de Git Bash est de faire un contrôle des versions de code de manière efficace, via les commandes de Git.

Avoir de la compétence en contrôle de versions est importante dans les projets complexes de programmation. En effet, programmer un robot peut être complexe selon le nombre de mécanismes à contrôler. L'utilisation du contrôle de versions permettra alors d'organiser le travail de programmation d'une manière plus structurée.

Malgré tout, il peut être assez compliqué de maîtriser le contrôle de versions. Et assez franchement, peu de personnes le maîtrisent à 100 %). Ce fut mon propre cas : je ne pense pas pouvoir prétendre maîtriser Git comme un expert, mais il m'est déjà arrivé plusieurs fois de ne pas comprendre ce que je faisais, ou de stresser à mort avant de publier mon code, juste parce que je ne comprenais pas comment fonctionnait exactement la gestion de versions.

Cependant, au fur et à mesure de votre cheminement en tant qu'ingénieur, programmeur (ou adulte responsable ? :sweat_smile:), je sais que vous aurez l'occasion de vous pratiquer et de développer par vous même des habitudes qui vous permettront de bien gérer vos versions de code.

Mais pour l'instant, je vais me contenter de vous expliquer comment installer Git et configurer GitLab pour une première utilisation de Git (et pour commencer à vous familiariser à une console de Linux).

### Téléchargement

Assez discuté, passons à l'installation.

1. Rendez-vous sur le [site web de Git](https://git-scm.com/)
2. Cliquez sur _Download <numéro-de-version> for Windows_, dans l'écran d'en bas à droite.

    ![Téléchargment de Git](figs/git/git-telechargement.png)

3. Exécutez l'installateur

### Installation

1. Il faut tout d'abord autoriser l'installation de _Git for Windows_.

    ![InstallationGit01](figs/git/installation/installation_git_01.png)

2. Ensuite, acceptez la licence (lisez-la si vous voulez :grin: !), en cliquant sur <kbd>Next ></kbd>.

    ![InstallationGit02](figs/git/installation/installation_git_02.png)

3. Pas de changements à signaler sur cette page. Cliquez sur <kbd>Suivant ></kbd>.

    ![InstallationGit03](figs/git/installation/installation_git_03.png)

4. Voilà le moment de passer à l'action ! Cochez la case `Check daily for Git for Windows updates` pour que _Git for Windows_ (l'implémentation de Git pour Windows) vérifie régulièrement des mises à jour.

    Il est aussi important de vérifier que la case `Git Bash Here` soit bel et bien cochée. Cette case active la possibilité d'ouvrir `Git Bash` à partir de n'importe quel dossier.

    ![InstallationGit04](figs/git/installation/installation_git_04.png)

5. Comme avant, pas de changements à signaler sur cette page. Cliquez sur <kbd>Next ></kbd>.

    ![InstallationGit05](figs/git/installation/installation_git_05.png)

6. Et là, on est rendu à une étape importante. Il faut choisir le bon éditeur de texte, pour que Git ouvre le bon éditeur lorsqu'il le faudra. Choisissez l'option `Use Visual Studio Code as Git's default editor`, pour que vous puissiez utiliser `VS Code` comme éditeur de texte par défaut.

    ![InstallationGit06](figs/git/installation/installation_git_06.png)

7. Choisissez l'option `Use Git from Git Bash only`. Cela évitera de changer la variable système PATH (oui, la même qui fut modifié en installant Python et VS Code)

    ![InstallationGit07](figs/git/installation/installation_git_07.png)

8. Choisissez l'option par défaut `Use the OpenSSL library`. Nous voulons garder des composantes libres et ouvertes (_Open_) dans notre installation !

    ![InstallationGit08](figs/git/installation/installation_git_08.png)

9. Choisissez l'option `Checkout Windows-style, commit Unix-style line endings`. Cela évitera des problèmes s'il faut travailler sur des fichiers préalablement modifiés sur un système d'exploitation de type Unix (si, par exemple, vous collaborez avec des amis qui utilisent Linux ou macOS, ou si vous travaillez aussi avec des _Raspberry Pis_ fonctionnant avec Linux).

    ![InstallationGit09](figs/git/installation/installation_git_09.png)

10. Choisissez l'option `Use MinTTY (the default terminal of MSYS2)`. Vous pourrez alors avoir un Git Bash qui imite les fonctionnalités d'un terminal sur Linux.

    ![InstallationGit10](figs/git/installation/installation_git_10.png)

11. Cochez les cases :

    * `Enable file system caching` pour que vos commandes Git soient plus performantes.
    * `Enable symbolic links` pour que _Git Bash_ puisse avoir des liens symboliques, une fonctionnalité non disponible sur Windows :cry:, sauf en cochant la case :white_check_mark:.

    Décochez la case `Enable Git Credential Manager`. Le Git Credential Manager est un logiciel de Microsoft pour générer des interfaces de Microsoft permettant de se connecter aux produits Microsoft. Cela est inutile si vous considérez, comme nous, éviter le plus que possible la dépendance à `Microsoft`, surtout pour la programmation (Mis à part VS Code :sweat_smile:).

    ![InstallationGit11](figs/git/installation/installation_git_11.png)

12. N'ajoutez pas la fonctionnalité `-i / -p`, vu qu'elle n'est pas encore fiable. Il se peut qu'une version ultérieure de ce guide vous recommandera de l'activer, mais ce n'est pas le cas pour le moment.

    ![InstallationGit12](figs/git/installation/installation_git_12.png)

13. Il n'y a plus qu'à attendre que _Git for Windows_ s'installe.

    ![InstallationGit13](figs/git/installation/installation_git_13.png)

14. Et voilà, _Git for Windows_ est installé !

    ![InstallationGit14](figs/git/installation/installation_git_14.png)

15. Il est maintenant possible de lancer _Git Bash_ avec un clic droit dans un dossier.

    ![InstallationGit15](figs/git/installation/installation_git_15.png)

<!-- ### Configuration

1. Une fois VSCode installé, ouvrez le menu des extensions en cliquant sur le logo suivant dans le panneau de gauche :

    ![Logo des extensions](figs/vscode/vscode-extensions.png)

2. Dans la barre de recherche, cherchez pour `Python` et cliquez sur le premier résultat. Vous devriez voir la fenêtre suivante :

    ![Fenêtre de Python](figs/vscode/vscode-python-extension.png)

3. Installez l'extension en cliquant sur _Install_
4. Une fois l'installation complétée, cliquez sur _Reload to Activate_
5. Faites la commande <kbd>Ctrl + Shift + P</kbd>. Vous verrez une barre de recherche en haut de votre fenêtre. Il s'agit de ce qu'on appelle _Palette de commandes_ (_command palette_, en anglais). Cette palette sera fort utile pour configurer VS Code pour qu'il soit à votre goût. Retenez bien la commande <kbd>Ctrl + Shift + P</kbd> !
6. Pour fermer la Command Palette, appuyez sur la touche Échap : <kbd>Esc</kbd>
7. Réouvrez la Command Palette et cherchez pour `interpret`. Vous devriez voir une option `Python : Sélectionner l'interpréteur` ou `Python : Select Interpreter` :

    ![Sélection de l'interpréteur](figs/vscode/vscode-python-select-interpreter.png)

8. Appuyez sur <kbd>Enter</kbd>. Vous trouverez alors une liste d'interpréteurs Python.
9. Vu que vous venez juste d'installer Python, vous ne devrez avoir qu'une seule option (dans mon cas, `Python 3.7.2 64-bit`). Sélectionnez cette option et appuyez sur <kbd>Enter</kbd>.
10. Voilà ! Vous avez configuré votre interpréteur Python ! Maintenant, comment écrire du code et l'exécuter ? Il faut d'abord configurer un raccourci clavier pour exécuter des fichiers Python. Pour cela, cliquez sur l'engrenage en bas à gauche et cliquez sur `Keyboard Shortcuts` :

    ![Raccourcis clavier](figs/vscode/vscode-settings-keyboard.png)

11. Sous la barre de recherche, en haut de la page, cliquez sur `keybindings.json`. Vous y verrez deux fichiers ouverts l'un à côté de l'autre :

    ![keybindings.json](figs/vscode/vscode-keybindings.png)

12. Ajoutez les paramètres suivants dans le fichier `keybindings.json` de droite :

    ```json
    // Place your key bindings in this file to overwrite the defaultsauto[]
    [
        {
            "key": "ctrl+alt+p",
            "command": "python.execInTerminal"
        }
    ]
    ```

13. Sauvegardez le fichier `keybindings.json` et fermez les deux fichiers de configuration

### Vérification

Maintenant que tout est configuré pour exécuter des scripts Python, il est temps d'essayer un petit script.

#### Dans MacOS

Sur macOS, ouvrez la Command Palette et tapez `shell command`. Sélectionnez l'option `Install 'code' command in PATH`.

Ouvrez une session de terminal et tapez les commandes suivantes :

```sh
cd ~
mkdir -p ~/code/GRUM/introduction-grum/python-vscode
cd ~/code/GRUM/introduction-grum/python-vscode
code .
```

Rendez-vous à la section [Test de script](#test-de-script)

#### Dans Windows

Allez dans le dossier `C:\Users\<votre-nom-d'utilisateur>`
(Dans mon cas, il s'agit de `C:\Users\DamLa`).

Tapez `cmd` dans la barre d'adresse de l'Explorateur Windows et appuyez sur <kbd>Enter</kbd>. Un invite de commande ouvrira. Tapez-y les commandes suivantes :

```powershell
mkdir .\code\GRUM\introduction-grum\python-vscode
cd .\code\GRUM\introduction-grum\python-vscode
code .
```

Il se pourrait que la commande `code` ne fonctionne pas. Si c'est le cas, il faut malheureusement redémarrer votre ordinateur.

#### Test de script

Bon, bienvenue dans votre espace de travail ! Comme vous pouvez le constater, les commandes ont ouvert une fenêtre VSCode dans le dossier `python-vscode`.

Faites un clic droit dans le panneau de gauche et créez un nouveau fichier en cliquant sur `New File`. Appelez-le `helloworld.py`.

Dans ce fichier, tapez le code suivant :

```py
print("Hello World")
```

Sauvegardez ce fichier et appuyez sur <kbd>Ctrl + Alt + P</kbd>. Voilà ! Vous pouvez lire `Hello World` dans la console ! -->

Bravo ! Vous avez fini d'installer Git et de configurer GitLab !

Rendez-vous au [prochain tutoriel](./installation-platformio.md)
