# Installation de Python

<img align="right" height="100" src="figs/logos/logo-python.png" />

Python est un langage de programmation en source ouverte développé par la fondation _PSF_ (pour _Python Software Foundation_).

Ce langage est simple d'utilisation, permettant d'implémenter rapidement des algorithmes complexes. C'est pourquoi Python est souvent utilisé pour enseigner la programmation à des petites classes, pour prototyper des codes, pour faire de l'analyse de données ou pour automatiser des tâches.

Ce langage permet de programmer des scripts qui peuvent être exécutés sur les _Raspberry Pis_.

<p style="text-align:center">
<img src="figs/boards/board-raspberry-pi.jpg" height="200"/>
</p>

## Windows

### Téléchargement

Bon, assez discuté, passons à l'installation. Avant tout, il est important de comprendre que nous voulons installer Python 3, pas Python 2.7. Cela est dû au fait que Python 2.7 ne sera plus maintenu à partir de 2020, comme l'indique [cette horloge](https://pythonclock.org/).

1. Rendez-vous sur le [site web de Python](https://www.python.org/downloads/)
2. Cliquer sur le nom de la **dernière version de Python 3**

    > ![Sélection de la version de Python](figs/python/python-version.png)

3. Vous trouverez les liens des installeurs à la fin de la page.

   > ![Installateurs de Python](figs/python/python-installateur.png)

    * Vu que nous sommes sur Windows, nous avons l'embarras du choix, mais que veulent dire `x86-64` et `x86` ? Ces valeurs indiquent que votre ordinateur fonctionne respectivement en 64-bit et en 32-bit, ce qui veut dire que votre CPU opère avec 32 ou 64 bits. Si votre ordinateur est récent et puissant, il y a de bonnes chances qu'il fonctionne sur 64 bits.
    * Maintenant, quelle est la différence entre l'`Embeddable Zip File`, l'`Executable Installer` et le `Web-Based Installer` ?
        * L'`Embeddable Zip File` consiste à télécharger la totalité de Python dans un fichier ZIP. Nous voulons éviter de configurer manuellement Python.
        * L'`Executable Installer` est un fichier exécutable EXE. On peut voir que sa taille est plus grande que celle de l'autre installeur. Cela est dû au fait que télécharger ce fichier consiste à télécharger un exécutable contenant la totalité de Python, ce qui risque de prendre beaucoup de temps.
        * Le `Web-Based Installer` semble être la meilleur option. La taille du fichier est plus petite, ce qui veut dire que l'installation à partir de l'exécutable téléchargera les autres fichiers.
    * Téléchargez le fichier _**`Windows x86-64 web-based installer`**_.
        > ![Installateur Web](figs/python/python-installateur-web.png)

4. Exécutez l'installateur. Et c'est parti pour l'[installation](#installation) !

### Installation

1. À la première page, cliquez sur la case `Add Python 3.7 to PATH` ou l'équivalent de cette case. Puis cliquez sur `Customize installation`. Cette étape est importante pour pouvoir utiliser Python à partir de votre terminal.

    > ![InstallationPython01](figs/python/installation/installation_python_01.png)

2. Il n'y a pas grande modification à apporter à cette page. Appuyez sur <kbd>Next</kbd>.

    > ![InstallationPython02](figs/python/installation/installation_python_02.png)

3. Sur cette page, activez les cinq premières cases et changez le chemin d'installation à `C:\Python37`. Cela nous permettra d'accéder facilement à l'interpréteur si jamais nous avons un problème (notamment si `Python` n'est pas ajouté au `PATH`).

    > ![InstallationPython03](figs/python/installation/installation_python_03.png)

4. Autorisez l'installation de Python.

    > ![InstallationPython04](figs/python/installation/installation_python_04.png)

5. Il faut ensuite attendre pendant que Python s'installe.

    > ![InstallationPython05](figs/python/installation/installation_python_05.png)

6. Enfin, cliquez sur `Disable path length limit` pour permettre à Python de dépasser la limitation de Windows des chemins d'accès à 260 caractères.

    > ![InstallationPython06](figs/python/installation/installation_python_06.png)

7. Cliquez sur `Oui`.

    > ![InstallationPython07](figs/python/installation/installation_python_07.png)

8. Et voilà, Python est installé ! Rendez-vous à la [vérification de l'installation](#v%c3%a9rification-de-linstallation)

    > ![InstallationPython08](figs/python/installation/installation_python_08.png)

### Vérification de l'installation

Maintenant que Python est installé, ouvrez une console de commandes et tapez la commande suivante :

```bash
python --version
```

Vous devriez voir `Python` suivi de 3 numéros, comme sur l'image ci-dessous :

> ![Vérification de Python Web](figs/python/python-verification.png)

* **Si c'est le cas, bravo ! Vous avez bien installé Python !** Rendez-vous au [prochain tutoriel](./installation-vscode.md)

* **Sinon, référez-vous au [dépannage](#d%c3%a9pannage)**

## Dépannage

Il se peut que vous n'arrivez pas à utiliser la commande `python` si votre console de commandes ne la reconnaît pas. Si c'est le cas, fermez et réouvrez la console de commandes et réessayez.

Si cette procédure n'a rien fait, cela veut dire que Python n'est pas dans votre variable système PATH. Auquel cas, cherchez pour "_Modifier les variables d'environnement système_" dans votre menu Démarrer et cliquez sur le bouton _Variables d'Environnement_:

> <img src="figs/python/python-depannage-proprietes.png" height="400" alt="Propriétés système"/>

* Dans _Variables utilisateur_, sélectionnez la variable **Path** et cliquez sur _Modifier_ :

> ![Modifier Path](figs/python/python-depannage-variables-modifier.png)

* Ajoutez un espace à la variable avec le bouton _Nouveau_ et mettez-y le chemin vers Python (dans mon cas, `C:\Python37\Scripts`). Déplacez cette variable au haut de la fenêtre.
* Ajoutez un espace avec un deuxième chemin vers Python (dans mon cas, `C:\Python37`)
* Le résultat final devrait ressembler à ceci (dans mon cas, `Python37`) :

> ![Chemins vers Python 3.7](figs/python/python-depannage-variables-paths.png)

* Cliquez sur OK dans les 3 fenêtres qui furent ouvertes durant la manipulation et revérifiez votre installation de Python.
