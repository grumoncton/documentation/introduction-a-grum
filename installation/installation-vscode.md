# Installation de Visual Studio Code

<img align="right" height="100" src="figs/logos/logo-vscode.png" />

Visual Studio Code (aussi connu comme _VS Code_) est un éditeur de code extensible multiplateforme en source ouverte développé par _Microsoft_. En d'autres mots, c'est un logiciel qui :

* peut fonctionner sur Windows, Linux et macOS
* permet d'éditer des fichiers de code
* est en source ouverte
* contient plusieurs extensions pouvant augmenter ses capacités

Nous utiliserons cet éditeur pour programmer en Python et pour utiliser l'extension PlatformIO, ce qui nous permettra de programmer des microcontrôleurs.

Mais pour l'instant, je vais me contenter de vous expliquer comment installer et configurer VS Code.

## Windows

### Téléchargement

Assez discuté, passons à l'installation.

1. Rendez-vous sur le [site web de VS Code](https://code.visualstudio.com/)
2. Cliquez sur _Download for Windows_
3. Exécutez l'installateur

### Installation

1. À la première page, acceptez la licence (lisez-la si vous voulez :grin: !).

    ![InstallationVSCode01](figs/vscode/installation/installation_vscode_01.png)

2. Pas de changements à signaler sur cette page. Cliquez sur <kbd>Suivant ></kbd>.

    ![InstallationVSCode02](figs/vscode/installation/installation_vscode_02.png)

3. Ici aussi, pas de changements à signaler sur cette page. Cliquez sur <kbd>Suivant ></kbd>.

    ![InstallationVSCode03](figs/vscode/installation/installation_vscode_03.png)

4. Voilà le moment de passer à l'action ! Activez les quatre dernières cases à cocher. Elles ont chacune leur propre utilité.

    * `Ajouter l'action "Ouvrir avec Code" au menu contextuel de fichier` : Il sera possible d'ouvrir un fichier avec VS Code, en faisant un clic droit sur celui-ci.
    * `Ajouter l'action "Ouvrir avec Code" au menu contextuel de répertoire` : Il sera possible d'ouvrir un dossier avec VS Code, en faisant un clic droit sur celui-ci.
    * `Inscrire Code en tant qu'éditeur pour les types de fichier pris en charge` : VS Code sera le logiciel par défaut pour ouvrir les fichiers de code.
    * `Ajouter à PATH (disponible après le redémarrage)` : Il sera possible d'ouvrir VS Code à partir d'un terminal, avec la commande `code`.

    ![InstallationVSCode04](figs/vscode/installation/installation_vscode_04.png)

5. Il n'y a plus qu'à attendre pendant que _Visual Studio Code_ s'installe.

    ![InstallationVSCode05](figs/vscode/installation/installation_vscode_05.png)

6. Et voilà, _Visual Studio Code_ est installé !

    ![InstallationVSCode06](figs/vscode/installation/installation_vscode_06.png)

7. Il est maintenant possible de lancer _Visual Studio Code_ avec un clic droit dans un dossier.

    ![InstallationVSCode07](figs/vscode/installation/installation_vscode_07.png)

### Configuration

1. Une fois VSCode installé, ouvrez le menu des extensions en cliquant sur le logo suivant dans le panneau de gauche :

    ![Logo des extensions](figs/vscode/vscode-extensions.png)

2. Dans la barre de recherche, cherchez pour `Python` et cliquez sur le premier résultat. Vous devriez voir la fenêtre suivante :

    ![Fenêtre de Python](figs/vscode/vscode-python-extension.png)

3. Installez l'extension en cliquant sur _Install_
4. Une fois l'installation complétée, cliquez sur _Reload to Activate_
5. Faites la commande <kbd>Ctrl + Shift + P</kbd>. Vous verrez une barre de recherche en haut de votre fenêtre. Il s'agit de ce qu'on appelle _Palette de commandes_ (_command palette_, en anglais). Cette palette sera fort utile pour configurer VS Code pour qu'il soit à votre goût. Retenez bien la commande <kbd>Ctrl + Shift + P</kbd> !
6. Pour fermer la Command Palette, appuyez sur la touche Échap : <kbd>Esc</kbd>
7. Réouvrez la Command Palette et cherchez pour `interpret`. Vous devriez voir une option `Python : Sélectionner l'interpréteur` ou `Python : Select Interpreter` :

    ![Sélection de l'interpréteur](figs/vscode/vscode-python-select-interpreter.png)

8. Appuyez sur <kbd>Enter</kbd>. Vous trouverez alors une liste d'interpréteurs Python.
9. Vu que vous venez juste d'installer Python, vous ne devrez avoir qu'une seule option (dans mon cas, `Python 3.7.2 64-bit`). Sélectionnez cette option et appuyez sur <kbd>Enter</kbd>.
10. Voilà ! Vous avez configuré votre interpréteur Python ! Maintenant, comment écrire du code et l'exécuter ? Il faut d'abord configurer un raccourci clavier pour exécuter des fichiers Python. Pour cela, cliquez sur l'engrenage en bas à gauche et cliquez sur `Keyboard Shortcuts` :

    ![Raccourcis clavier](figs/vscode/vscode-settings-keyboard.png)

11. Sous la barre de recherche, en haut de la page, cliquez sur `keybindings.json`. Vous y verrez deux fichiers ouverts l'un à côté de l'autre :

    ![keybindings.json](figs/vscode/vscode-keybindings.png)

12. Ajoutez les paramètres suivants dans le fichier `keybindings.json` de droite :

    ```json
    // Place your key bindings in this file to overwrite the defaultsauto[]
    [
        {
            "key": "ctrl+alt+p",
            "command": "python.execInTerminal"
        }
    ]
    ```

13. Sauvegardez le fichier `keybindings.json` et fermez les deux fichiers de configuration

### Vérification

Maintenant que tout est configuré pour exécuter des scripts Python, il est temps d'essayer un petit script.

#### Dans MacOS

Sur macOS, ouvrez la Command Palette et tapez `shell command`. Sélectionnez l'option `Install 'code' command in PATH`.

Ouvrez une session de terminal et tapez les commandes suivantes :

```sh
cd ~
mkdir -p ~/code/GRUM/introduction-grum/python-vscode
cd ~/code/GRUM/introduction-grum/python-vscode
code .
```

Rendez-vous à la section [Test de script](#test-de-script)

#### Dans Windows

Allez dans le dossier `C:\Users\<votre-nom-d'utilisateur>`
(Dans mon cas, il s'agit de `C:\Users\DamLa`).

Tapez `cmd` dans la barre d'adresse de l'Explorateur Windows et appuyez sur <kbd>Enter</kbd>. Un invite de commande ouvrira. Tapez-y les commandes suivantes :

```powershell
mkdir .\code\GRUM\introduction-grum\python-vscode
cd .\code\GRUM\introduction-grum\python-vscode
code .
```

Il se pourrait que la commande `code` ne fonctionne pas. Si c'est le cas, il faut malheureusement redémarrer votre ordinateur.

#### Test de script

Bon, bienvenue dans votre espace de travail ! Comme vous pouvez le constater, les commandes ont ouvert une fenêtre VSCode dans le dossier `python-vscode`.

Faites un clic droit dans le panneau de gauche et créez un nouveau fichier en cliquant sur `New File`. Appelez-le `helloworld.py`.

Dans ce fichier, tapez le code suivant :

```py
print("Hello World")
```

Sauvegardez ce fichier et appuyez sur <kbd>Ctrl + Alt + P</kbd>. Voilà ! Vous pouvez lire `Hello World` dans la console !

Bravo ! Vous avez fini d'installer VSCode et Python dans VSCode !

Rendez-vous au [prochain tutoriel](./installation-git-gitlab.md)
