# Introduction au GRUM

> Rien ne sert de courir, il faut partir à point

## Un peu de vocabulaire

Bienvenue à ce tutoriel d'introduction au GRUM. Le but principal de ce tutoriel est de vous aider à installer les logiciels que nous utilisons pour programmer les robots du GRUM. Il vous permettra de comprendre comment les configurer et comment les utiliser. Il est à noter que ce tutoriel fut écrit en janvier 2019. Ce faisant, si le GRUM adopte d'autres logiciels, ce guide ne servira plus.

Tout d'abord, bienvenue sur GitLab. GitLab est un site Web permettant d'héberger et de télécharger des dossiers de projets contenant du code. Chaque dossier de projet est appelé _**repository**_ (ou _repo_, pour faire court). Chaque dossier de projet peut contenir un fichier de documentation appelé _**README**_ (_LISEZMOI_, en français) qui est généralement écrit en _Markdown_, d'où l'extension _.md_. C'est justement le fichier que vous êtes en train de lire présentement.

Assez parlé de GitLab pour maintenant. Nous y reviendrons plus tard. Pour l'instant, l'important est de comprendre que :

* Les dossiers de projets contenant les codes sont appelés _Repositories_.
* Chaque repository peut contenir un fichier Markdown appelé _README.md_. C'est ce fichier qui est affiché par GitLab quand vous arrivez dans un _repository_.

## Installation des logiciels

Nous suivrons l'ordre suivant pour installer les logiciels :

1. [Installation de Python](installation/installation-python.md)
2. [Installation de VS Code](installation/installation-vscode.md)
3. [Installation de Git et configuration de GitLab](installation/installation-git-gitlab.md)
4. [Installation de PlatformIO IDE]()
